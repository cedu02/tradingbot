from gym.envs.registration import register

register(
    id='binance_env-v0',
    entry_point='crypto_env.envs:BinanceEnv',
)