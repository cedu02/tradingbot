import math
import gym
import numpy as np
from gym import spaces
from crypto_env.envs.coin import Coin
from datetime import datetime, timedelta
import tensorflow as tf
import time
import csv

COIN_FEATURE = 11
ADDITIONAL_FEATURE = 6
BINANCE_FEE = 0.001
SLIPPAGE = 0.0005

INITIAL_BALANCE = 1000
MAX_ACCOUNT_BALANCE = INITIAL_BALANCE * 1.1

MAX_RUN_MINUTES = 10
MAX_TRANSACTIONS = MAX_RUN_MINUTES * 60 / 10 * 100

COINS = ['BTC']

class BinanceEnv(gym.Env):

    """A binance crypto trading environment for OpenAI gym"""
    metadata = {'render.modes': ['human']}

    def __init__(self):
        # coins: number of coins it can trade. (ticker)
        super(BinanceEnv, self).__init__()

        self.reward_range = (0, MAX_ACCOUNT_BALANCE)

        self.current_usd_balance = INITIAL_BALANCE
        self.balance = INITIAL_BALANCE

        self.buy_orders = []
        self.sell_orders = []

        self.result_data = []

        self.episode = 0
        self.evaluation_run = False
        self.fee = BINANCE_FEE
        self.time_start = datetime.utcnow()

        # Actions of the format Buy x%, Sell x%, Hold, Train.
        self.action_space = spaces.MultiDiscrete([len(COINS), 3])

        # Prices contain the predictions for each coin made by the LSTM nets and current state of trading
        self.observation_space = spaces.Box(low=0,
                                            high=1,
                                            shape=(COIN_FEATURE * len(COINS) + ADDITIONAL_FEATURE, ),
                                            dtype=np.float16)

        # Create prediction nets for coins
        self.coins = []
        for coin in COINS:
            self.coins.append(Coin(coin, True))

    def _next_observation(self):
        obs = []
        current_usd_value = self.balance

        for coin in self.coins:
            price = coin.get_current_bid()
            obs.extend(coin.read_latest_prediction())
            order_book_normal = coin.get_current_bid_ask_normalized()
            obs.extend([order_book_normal[0], order_book_normal[1]])
            current_usd_value = current_usd_value + (price * coin.holding)

        for coin in self.coins:
            price = coin.get_current_bid()
            value = (price * coin.holding) / MAX_ACCOUNT_BALANCE
            obs.append(value)

        obs.append(self.balance / MAX_ACCOUNT_BALANCE)
        obs.append(current_usd_value / MAX_ACCOUNT_BALANCE)
        obs.append(len(self.buy_orders) / MAX_TRANSACTIONS)
        obs.append(len(self.sell_orders) / MAX_TRANSACTIONS)
        obs.append((len(self.buy_orders) + len(self.sell_orders)) / MAX_TRANSACTIONS)

        time_til_end = (self.time_start + timedelta(minutes=30)) - datetime.utcnow()
        obs.append(time_til_end.seconds / (30 * 60))
        self.current_usd_balance = current_usd_value

        for i, ob in enumerate(obs):
            # too big
            if ob > 1:
                obs[i] = 1

            # too small
            if ob < 0:
                obs[i] = 0

        return np.array(obs, )

    def _take_action(self, action):
        coin = self.coins[math.floor(action[0])]
        task = action[1]
        amount = 1
        reward = 0.1

        # buy
        if task <= 1:
            if self.balance > 20:
                self._simulate_buy(coin, amount)
            else:
                reward = 0

        # sell
        elif task <= 2:
            if coin.holding > 0:
                reward = self._simulate_sale(coin, amount)
                if reward > 0:
                    reward = 1 + 100 * reward
                else:
                    reward = 0.1

        return reward

    def step(self, action):
        # take action and get reward
        reward = self._take_action(action)
        obs = self._next_observation()
        # self.current_step += 1
        # Check if a end is reached
        one_percent = (INITIAL_BALANCE / 100)
        low_balance = self.current_usd_balance <= (INITIAL_BALANCE - (one_percent * 20))
        max_time = ((datetime.utcnow() - self.time_start).total_seconds() / 60.0) >= MAX_RUN_MINUTES
        max_transactions = (len(self.buy_orders) + len(self.sell_orders)) >= MAX_TRANSACTIONS

        done = low_balance or max_time or max_transactions

        # log run statistics
        if done:
            self.result_data.append({"end_reason": self._get_end_reason(low_balance,  max_time),
                                     "result": self.current_usd_balance - INITIAL_BALANCE,
                                     "buy_orders": len(self.buy_orders),
                                     "sell_orders": len(self.sell_orders),
                                     "duration": (datetime.utcnow() - self.time_start).total_seconds() / 60.0})

        return obs, reward, done, {}

    def _simulate_sale(self, coin, amount):
        money_return = coin.simulate_sell(amount, BINANCE_FEE, SLIPPAGE)
        last_buy_order = self.buy_orders[-1]
        reward = money_return - last_buy_order["usd_value"]
        self.sell_orders.append({"usd_value": money_return, "time": datetime.utcnow()})

        self.balance += money_return
        coin.holding -= (coin.holding * amount)
        return reward

    def _simulate_buy(self, coin, amount):
        coin_amount, price = coin.simulate_buy(amount, self.balance, BINANCE_FEE, SLIPPAGE)
        coin.holding += coin_amount
        self.balance = 0 # because we buy all always now
        self.buy_orders.append({"usd_value": price * coin_amount , "time": datetime.utcnow()})

    def reset(self):
        self.time_start = datetime.utcnow()

        self.buy_orders = []
        self.sell_orders = []
        self.result_data = []

        self.current_usd_balance = INITIAL_BALANCE
        self.balance = INITIAL_BALANCE

        # reset coins
        for coin in self.coins:
            coin.holding = 0

        return self._next_observation()

    def _get_end_reason(self, low_balance, max_time):
        if low_balance:
            return "Low Balance"
        elif max_time:
            return "Time"


    def render(self, mode='human'):
        print("USDT Total: ", self.current_usd_balance)

