import json
from os import path

import numpy as np
import redis
import requests
from pandas import DataFrame
from pandas import concat
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras import models
from tensorflow.python.keras import Sequential
from tensorflow.python.keras.layers import LSTM, Dense

BINANCE_API_URL = "https://api3.binance.com/api/v3/"
BINANCE_KLINE_ENDPOINT = "klines"
BINANCE_PRICE_ENDPOINT = "ticker/price"
BINANCE_ORDERBOOK_ENDPOINT = "depth"

SYMBOL_PARAM = "?symbol="
INTERVAL_PARAM = "&interval="
LIMIT_PARAM = "&limit="


# convert series to supervised learning
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


def convert_json(rows, json):
    data = []
    for x in range(0, rows):
        data.append([np.float64(json[x][1]),
                     np.float64(json[x][2]),
                     np.float64(json[x][3]),
                     np.float64(json[x][4]),
                     np.float64(json[x][5]),
                     np.float64(json[x][7]),
                     json[x][8],
                     np.float64(json[x][9]),
                     np.float64(json[x][10])])
    return data


class Coin:

    def __init__(self, coin, no_model=False):
        self.name = coin
        self.symbol = coin + "USDT"

        self.kline_endpoint = BINANCE_API_URL + BINANCE_KLINE_ENDPOINT + SYMBOL_PARAM + coin + "USDT" + INTERVAL_PARAM + "1m"
        self.price_endpoint = BINANCE_API_URL + BINANCE_PRICE_ENDPOINT + SYMBOL_PARAM + coin + "USDT"
        self.order_book_endpoint = BINANCE_API_URL + BINANCE_ORDERBOOK_ENDPOINT + SYMBOL_PARAM + coin + "USDT" + LIMIT_PARAM + "5"
        self.redis_book_key = self.name.lower() + "usdt" + "book"

        if no_model == False:
            self.model_path = "models/" + coin
            if path.exists(self.model_path):
                self.model = models.load_model(self.model_path)
            else:
                self.create()

        self.holding = 0
        self.accuracy = [0, 0, 0, 0]
        self.last_price = 0
        self.last_kline = []
        self.last_time_stamp = 0

        self.scaler = 0

        # initialize redis
        self.redis = redis.Redis(host='localhost', port=6379, db=0)

    def get_next_kline(self):
        r = requests.get(self.kline_endpoint)

        if r.status_code != 200:
            dataset = self.last_kline
        else:
            dataset = convert_json(len(r.json()), r.json())

        return dataset

    def normalize_data(self, kline):
        # normalize data
        self.scaler = MinMaxScaler(feature_range=(0, 1))
        min_in_data = np.min(np.array(kline), axis=0)
        max_in_data = np.max(np.array(kline), axis=0)

        max_key = self.name.lower() + "usdt" + "max"
        max = float(self.redis.get(max_key))
        if max is None:
            return

        min_key = self.name.lower() + "usdt" + "min"
        min = float(self.redis.get(min_key))
        if min is None:
            return

        self.scaler.fit(
            [[min, min, min, min, min_in_data[4], min_in_data[5], min_in_data[6], min_in_data[7], min_in_data[8]],
             [max, max, max, max, max_in_data[4], max_in_data[5], max_in_data[6], max_in_data[7], max_in_data[8]]])

        return self.scaler.transform(kline)

    def get_real_values(self, prediction):
        scaler = MinMaxScaler(feature_range=(0, 1))
        max_key = self.name.lower() + "usdt" + "max"
        max = float(self.redis.get(max_key))
        if max is None:
            return

        min_key = self.name.lower() + "usdt" + "min"
        min = float(self.redis.get(min_key))
        if min is None:
            return

        scaler.fit(
            [[min, min, min, min, min, min, min, min],
             [max, max, max, max, max, max, max, max]])

        value = scaler.inverse_transform([prediction])
        return value[0]

    def create_next_prediction(self):
        current_price = self.get_current_bid()
        dataset = []
        try:
            r = requests.get(self.kline_endpoint)
            raw_kline_data = r.json()
            self.last_time_stamp = raw_kline_data[499][0]
            if r.status_code != 200:
                dataset = self.last_kline
            else:
                dataset = convert_json(len(r.json()), raw_kline_data)
        except:
            dataset = self.last_kline

        self.last_kline = dataset

        dataset = self.normalize_data(dataset)
        if dataset is None:
            return

        input_data = np.array([dataset[499]]).reshape((1, 1, 9))
        predictions = self.model.predict(input_data)
        current = dataset[499]

        # return current and prediction
        result = np.zeros(8)
        result[:-4] = current[:-5]
        result[4:] = predictions

        return result

    def read_latest_prediction(self):
        predictions = json.loads(self.redis.get(self.name.lower() + "kline"))
        if predictions is None:
            predictions = np.zeros(8).tolist()

        return predictions

    def get_next_prediction_real_values(self):
        r = requests.get(self.kline_endpoint)

        if r.status_code != 200:
            dataset = self.last_kline
        else:
            dataset = convert_json(len(r.json()), r.json())

        self.last_kline = dataset

        # normalize data for model
        dataset = np.array(dataset)
        scaler = MinMaxScaler(feature_range=(0, 1))
        dataset = scaler.fit_transform(dataset)
        data = np.array([dataset[499]]).reshape((1, 1, 9))
        predictions = self.model.predict(data)

        prediction_values = np.zeros(9, )
        prediction_values[:-5] = predictions
        prediction_real = scaler.inverse_transform([prediction_values])
        current_real = self.last_kline[499][:-5]

        # return current and prediction
        result = np.zeros(8)
        result[:-4] = prediction_real[0][:-5]
        result[4:] = current_real

        return result

    def update_holding(self, count):
        self.holding = self.holding + count

    def get_current_bid(self):
        value = json.loads(self.redis.get(self.redis_book_key))
        return float(value["b"])

    def get_current_bid_ask_normalized(self):
        value = json.loads(self.redis.get(self.redis_book_key + "normal"))
        return (float(value[0]), float(value[1]))

    def get_current_ask(self):
        value = json.loads(self.redis.get(self.redis_book_key))
        return float(value["a"])

    def create(self):
        model = Sequential()
        model.add(LSTM(200, input_shape=(1, 9)))
        model.add(Dense(500))
        model.add(Dense(4))
        model.compile(loss='mean_squared_error', optimizer='adam')
        self.model = model
        self.train(1, 1)
        model.save("models/" + self.name)

    def train(self, epochs, batch_size):
        r = requests.get(self.kline_endpoint + LIMIT_PARAM + "1000")
        if r.status_code != 200:
            dataset = self.last_kline
        else:
            dataset = convert_json(len(r.json()), r.json())

        self.last_kline = dataset

        # normalize data
        dataset = self.normalize_data(dataset)
        if dataset is None:
            return

        # create time series for supervised learning.
        reframed = series_to_supervised(dataset, 1, 1)
        reframed.drop(reframed.columns[[13, 14, 15, 16, 17]], axis=1, inplace=True)
        dataset = reframed.values

        train_x, train_y = dataset[:, :-4], dataset[:, -4:]
        # reshape input to be 3D [samples, timesteps, features]
        train_x = train_x.reshape((train_x.shape[0], 1, train_x.shape[1]))

        self.model.fit(train_x, train_y, epochs=epochs, batch_size=batch_size, verbose=1, shuffle=False)
        self.model.save("models/" + self.name)

    def simulate_buy(self, amount, balance, fee, slippage):
        current_ask_price = self.get_current_ask()
        price = current_ask_price + (current_ask_price * slippage)
        buy_amount = balance / price - (balance / price * fee)
        return (buy_amount, price)

    def simulate_sell(self, amount, fee, slippage):
        current_bid_price = self.get_current_bid()
        price = current_bid_price - (amount * self.holding * fee) - (current_bid_price * slippage)
        return self.holding * price

    def reaload_model(self):
        self.model = models.load_model(self.model_path)
