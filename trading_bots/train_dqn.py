from crypto_env.envs import BinanceEnv
from tensorforce import Runner, Environment, Agent
import pandas as pd
import matplotlib.pyplot as plt

env = BinanceEnv()
environment = Environment.create(
    environment='gym',
    level='binance_env-v0',
    visualize=False
)

# Instantiate a Tensorforce agent
agent = Agent.create(
    agent='dqn',
    environment=environment,
    memory=1000,
    exploration=dict(type='exponential',
                     unit='timesteps',
                     num_steps=1000,
                     initial_value=0.9,
                     decay_rate=0.05),
    learning_rate=dict(type='exponential',
                       unit='timesteps',
                       num_steps=1000,
                       initial_value=0.01,
                       decay_rate=0.5),
    batch_size=5,
    summarizer=dict(
        directory='log/dqn'
    )
)

runner = Runner(
    agent=agent,
    environment=environment,
)

runner.run(num_episodes=100)
env.evaluation_run = True
runner.run(num_episodes=3, evaluation=True, save_best_agent='agents/')

runner.close()

for result in env.result_data:
    print(result)
