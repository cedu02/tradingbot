import itertools
import json
import os
import gym
from tensorforce import Runner
import numpy as np
import matplotlib.pyplot as plt


class Trainer:
    def __init__(self):
        self.max_episode_timesteps = 30000
        self.rewards_list = []
        self.env = gym.make('crypto_env:binance_env-v0')
        self.param_grid_list = {
            "batch_size": [5, 10],
            "learning_rate": [1e-3, 1e-2],
            "discount": [0.01, 0.1],
            "exploration_step": [0.1, 0.01],
            "exploration_start": [0.9, 0.5],
            "exploration_number_steps": [1000, 100],
            "l2_regularization": [0.1, 0.01, 0.001],
            "network": ["auto"],
            "size": [32, 64, 128],
            "depth": [2, 4, 6, 8],
            "rnn": [False, 20, 50, 100]
        }

    def train(self):

        # Initialize lists to gather the results over the different combinations
        max_reward = -10

        lists = self.param_grid_list.values()
        param_combinations = list(itertools.product(*lists))
        total_param_combinations = len(param_combinations)
        print("Number of combinations", total_param_combinations)

        random_selection = np.random.randint(0, len(param_combinations)-1, 10)
        # Loop over the different parameters combination
        for i in random_selection:
            params = param_combinations[i]

            print("Combination", i, "/", total_param_combinations)
            name = "combo_" + str(i)

            # Fill the parameters dictionnary with the current parameters combination
            param_grid = {}
            for param_index, param_name in enumerate(self.param_grid_list):
                param_grid[param_name] = params[param_index]

            # Create the agent with the current parameters combination
            agent = dict(
                agent='ddqn',
                memory=self.max_episode_timesteps * 2,
                network=dict(type='auto',
                             size=param_grid["size"],
                             depth=param_grid["depth"],
                             rnn=param_grid["rnn"]),
                batch_size=param_grid["batch_size"],
                learning_rate=param_grid["learning_rate"],
                exploration=dict(
                    type='exponential',
                    unit='timesteps',
                    num_steps=1000,
                    initial_value=param_grid["exploration_start"],
                    decay_rate=param_grid["exploration_step"]
                ),
                discount=param_grid["discount"],
                saver=dict(directory='agents/' + name, frequency=10, max_checkpoints=5),
            )

            runner = Runner(
                agent=agent,
                environment=dict(environment='gym', level=self.env),
                max_episode_timesteps=self.max_episode_timesteps,
                num_parallel=25,
                remote='multiprocessing'
            )

            runner.run(num_episodes=500,
                       batch_agent_calls=True)

            runner.close()

            reward = (float(np.mean(runner.episode_rewards, axis=0)))

            if reward > max_reward:
                max_reward = reward

            # Create a folder for the i-th parameter combination if it doesn't exist
            try:
                # os.mkdir(os.path.join("env", "Graphs", str(i)))
                self.save_plot_image(1,
                                     timeX=np.linspace(1, runner.episodes, runner.episodes),
                                     timeY=runner.episode_rewards,
                                     time_x_label='Episode',
                                     time_y_label='Total reward',
                                     path=('agents/' + name))
            except:
                pass

        # Create a dictionnary of hyperparameters and their results
        print("Maximaler reward: ", max_reward)

    def save_plot_image(self,
                        pltNo,
                        timeX,
                        timeY,
                        time_x_label,
                        time_y_label,
                        path,
                        legend1=None,
                        title=None):

        plt.close(pltNo)
        fig = plt.figure(pltNo)
        fig.canvas.set_window_title(title)

        subplot1 = fig.add_subplot(1, 1, 1)

        if legend1 is not None:
            subplot1.plot(timeX, timeY, label=legend1)
        else:
            subplot1.plot(timeX, timeY)
        subplot1.set_ylabel(time_y_label)
        subplot1.set_xlabel(time_x_label)
        if legend1 is not None:
            subplot1.legend(loc='upper right')

        fig.tight_layout()
        fig.savefig(fname=path)


if __name__ == '__main__':
    Trainer().train()
