import gym

from trading_bots.tune import TensorforceWorker
import math
import os
import pickle
from hpbandster.core.nameserver import NameServer
from hpbandster.core.result import json_result_logger
from hpbandster.optimizers import BOHB
from tune import TensorforceWorker

host = 'localhost'
port = None

env = gym.make('crypto_env:binance_env-v0')

server = NameServer(run_id="trainer", working_directory='tuner', host=host, port=port)
nameserver, nameserver_port = server.start()

worker = TensorforceWorker(
    environment=env,
    max_episode_timesteps=10000,
    num_episodes=50,
    base=3,
    runs_per_round=(2, 5, 7, 10),
    num_parallel=5,
    run_id='worker',
    nameserver=nameserver,
    nameserver_port=nameserver_port,
    host=host
)
worker.run(background=True)
previous_result = None
result_logger = json_result_logger(directory='tuner', overwrite=True)

optimizer = BOHB(
    configspace=worker.get_configspace(),
    eta=3,
    min_budget=0.9,
    max_budget=math.pow(3, len((2, 5, 7, 10)) - 1),
    run_id='worker',
    working_directory='tuner',
    nameserver=nameserver,
    nameserver_port=nameserver_port,
    host=host,
    result_logger=result_logger,
    previous_result=previous_result
)

results = optimizer.run(n_iterations=2)
# optimizer.run(n_iterations=1, min_n_workers=1, iteration_kwargs={})
# min_n_workers: int, minimum number of workers before starting the run

optimizer.shutdown(shutdown_workers=True)
server.shutdown()

with open(os.path.join('tuner', 'results.pkl'), 'wb') as filehandle:
    pickle.dump(results, filehandle)

print('Best found configuration: {}'.format(
    results.get_id2config_mapping()[results.get_incumbent_id()]['config']
))
print('Runs:', results.get_runs_by_id(config_id=results.get_incumbent_id()))
print('A total of {} unique configurations where sampled.'.format(
    len(results.get_id2config_mapping())
))
print('A total of {} runs where executed.'.format(len(results.get_all_runs())))