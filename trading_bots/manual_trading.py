import json
import os

from crypto_env.envs import Coin

MIN_RISE = 1.01
FEE = 0.001
SLIPPAGE = 0.005

START_VALUE = 100
STOP_LOSS = 50
REQUEST_LIMIT = 1200


def simulate_sale(coin):
    price = coin.get_current_bid()
    fee = (coin.holding * price * FEE * SLIPPAGE)
    return (coin.holding * price) - fee


def expected_change(prediction, current_price):
    predicted_high = prediction[1]
    return predicted_high * 100 / current_price


# read configurations
config_path = os.path.join(os.path.dirname(__file__), "../config_files/coins.json")
coin_json = open(config_path)
coin_names = json.load(coin_json)
coins = {}
tusd_balance = START_VALUE
request_count = 0

for coin_name in coin_names["coins"]:
    coins[coin_name] = (Coin(coin_name), 0)

while True:
    change_predictions = {}
    for coin_name in coins:
        coin = coins[coin_name][0]
        price = coin.get_current_bid()
        prediction = coin.get_next_prediction_real_values()
        expected_price_change = expected_change(prediction, price)
        change_predictions[coin.name] = expected_price_change

        # decide to sell
        if coin.holding > 0:
            success = 100 / coins[coin.name][1] * price > 2

            if expected_price_change < 100 or prediction[1] < price or prediction[1] <= coins[coin.name][1] or success:
                return_usd = simulate_sale(coin)
                print("Sell", coin.name, " for: ", return_usd, " USD")
                if success:
                    print("Sold in plus", success)
                else:
                    print("sold because of loss")
                tusd_balance += return_usd
                coin.holding = 0

    if tusd_balance > 10:
        # decide to buy
        biggest_rice = 0
        biggest_name = ""
        for key in change_predictions:
            if biggest_name == "":
                biggest_name = key

            if change_predictions[key] > biggest_rice:
                biggest_rice = change_predictions[key]
                biggest_name = key

        coin = coins[biggest_name][0]
        if biggest_rice > 0.1:
            value = coin.simulate_buy(1, tusd_balance, FEE, SLIPPAGE)
            tusd_balance = 0
            coins[biggest_name][0].holding += value[0]
            coins[biggest_name] = (coins[biggest_name][0], value[1])

        if biggest_rice > MIN_RISE:
            value = coin.simulate_buy(1, tusd_balance, FEE, SLIPPAGE)
            tusd_balance -= value[1] * value[0]
            coins[biggest_name][0].holding += value[0]
            coins[biggest_name] = (coins[biggest_name][0], value[1])

    print("USD:", tusd_balance)
    for key in coins:
        print(key, coins[key][0].holding)
        if coins[key][0].holding > 0:
            print("Buy price: ", coins[key][1])
