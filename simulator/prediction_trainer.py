import datetime
import json
import os

# read configurations
from crypto_env.envs import Coin

config_path = os.path.join(os.path.dirname(__file__), "../config_files/coins.json")
coin_json = open(config_path)
coins = json.load(coin_json)

coin_list = []
coin_update_time = {}

for coin in coins['coins']:
    coin_list.append(Coin(coin))
    coin_update_time[coin] = datetime.datetime.utcnow()

# get next kline.
while True:
    for coin in coin_list:
        coin.train(100, 1)
