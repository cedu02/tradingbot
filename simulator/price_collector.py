import json
import os
import redis
from binance.client import Client
from binance.websockets import BinanceSocketManager
from sklearn.preprocessing import MinMaxScaler
import numpy as np


def handle_book_updates(msg):
    data = msg["data"]
    symbol = data["s"] + "book"
    if data["a"] is None or data["b"] is None:
        return
    r.set(symbol.lower(), json.dumps(data))

    max_key = data["s"].lower() + "max"
    max_value = float(data["a"]) * 1.5
    if not r.exists(max_key):
        r.set(max_key, max_value)
    else:
        max_value = float(r.get(max_key))

    min_key = data["s"].lower() + "min"
    min_value = float(data["b"]) / 1.5
    if not r.exists(min_key):
        r.set(min_key, min_value)
    else:
        min_value = float(r.get(min_key))

    scaler = MinMaxScaler()
    x_sample = [min_value, max_value]
    scaler.fit(np.array(x_sample)[:, np.newaxis])
    normalized = scaler.transform([[data["a"]], [data["b"]]])
    r.set(symbol.lower() + "normal", json.dumps([normalized[0][0], normalized[1][0]]))
    prices[data["s"].lower()] = (data["a"], normalized[0][0])
    print_prices()


def print_prices():
    text = ""
    for coin in coin_names:
        ask_price = str(round(float(prices[coin.lower() + "usdt"][0]), 3))
        ask_price_normalized = str(prices[coin.lower() + "usdt"][1])
        text = text + coin.lower() + ask_price + " / " + ask_price_normalized + " | "
    print(text, end='\r')


# read configurations
config_path = os.path.join(os.path.dirname(__file__), "../config_files/coins.json")
coin_json = open(config_path)
coin_names = json.load(coin_json)["coins"]

# initialize binance client
client = Client("", "")
bm = BinanceSocketManager(client)
prices = {}
books = []
for coin in coin_names:
    books.append(coin.lower() + "usdt" + "@bookTicker")
    prices[coin.lower() + "usdt"] = (0, 0)

# initialize redis
r = redis.Redis(host='localhost', port=6379, db=0)
partial_key = bm.start_multiplex_socket(books, handle_book_updates)
bm.start()
