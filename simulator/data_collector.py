import json
import os
from datetime import datetime, timedelta
import redis
from crypto_env.envs import Coin
import tensorflow as tf


def print_prices():
    text = ""
    for coin in coin_names:
        next_high = str(round(float(latest_prediction[coin]["prediction"][1]), 3))
        accuracy_text = str(latest_prediction[coin]["latest_accuracy"])
        text = text + coin.lower() + " " + next_high + " / " + accuracy_text + " | "
    print(text, end='\r')


# read configurations
config_path = os.path.join(os.path.dirname(__file__), "../config_files/coins.json")
coin_json = open(config_path)
coin_names = json.load(coin_json)["coins"]

# initialize redis
r = redis.Redis(host='localhost', port=6379, db=0)

coins = []
latest_prediction = {}

for coin in coin_names:
    coins.append(Coin(coin))

last_prediction_time = datetime.utcnow()
last_model_update = datetime.utcnow()

for coin in coins:
    p = coin.create_next_prediction()
    r.set(coin.name.lower() + "kline", json.dumps(p.tolist()))
    real_values = coin.get_real_values(p)

    latest_prediction[coin.name] = {"time": datetime.fromtimestamp(coin.last_time_stamp // 1000),
                                    "prediction": real_values[4:],
                                    "latest_accuracy": 0}

while True:
    if last_prediction_time <= datetime.utcnow() - timedelta(minutes=0.5):
        for coin in coins:
            p = coin.create_next_prediction()
            r.set(coin.name.lower() + "kline", json.dumps(p.tolist()))
            real_values = coin.get_real_values(p)
            kline_open_time = datetime.fromtimestamp(coin.last_time_stamp // 1000)

            if kline_open_time != latest_prediction[coin.name]["time"]:
                # calculate accuracy of high
                accuracy = (100 / latest_prediction[coin.name]["prediction"][1]) * real_values[1]
                latest_prediction[coin.name]["latest_accuracy"] = accuracy

                latest_prediction[coin.name]["time"] = kline_open_time
                latest_prediction[coin.name]["prediction"] = real_values[4:]
                print_prices()
        last_prediction_time = datetime.utcnow()

    if last_model_update <= datetime.utcnow() - timedelta(minutes=10):
        # to avoid memory overflow
        tf.keras.backend.clear_session()
        # load new model
        for coin in coins:
            coin.reaload_model()
        last_model_update = datetime.utcnow()
